#-------------------------------------------------
#
# Project created by QtCreator 2014-04-23T17:03:18
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleSniffer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sniffer.cpp \
    ethernet_packet.cpp \
    ip_packet.cpp \
    imagepacket.cpp

HEADERS  += mainwindow.h \
    ethernet_hdr.h \
    ip_hdr.h \
    tcp_hdr.h \
    sniffer.h \
    ethernet_packet.h \
    ip_packet.h \
    udp_hdr.h \
    imagepacket.h

LIBS += -lpcap

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc
