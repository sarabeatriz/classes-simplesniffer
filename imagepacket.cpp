#include "imagepacket.h"

/// \fn imagepacket::imagepacket()
/// \~English
/// \brief Construct a list of known image extensions.
/// \~Spanish
/// \brief Construye una lista de extensiones de imagenes conocida.
imagepacket::imagepacket()
{
    extensions << "jpg" << "jpeg" << "png" << "tiff" << "gif";
}

/// \fn bool imagepacket::isImage(string payload)
/// \~English
/// \brief Returns true if the http request payload received contains
/// or is a request to an image, and stores the image url
/// \param payload HTTP packet payload
/// \return true if the http request payload received contains
/// or is a request for an image
/// \~Spanish
/// \brief Devuelve cierto si la carga de la solicitud de HTTP recibida
/// contiene o es una solicitud a una imagen, y guarda la imagen
/// \param payload carga del paquete de HTTP
/// \return cierto si la carga de la solicitud de HTTP recibida contiene
/// o es una solicitud a una imagen
bool imagepacket::isImage(string payload){

    QStringList list = QString::fromStdString(payload).split("\r\n") ;
    QStringList line ;
    QString url ;
    if(list.size() >= 2){
           line = list[0].split(" ") ;
           if(line.size() < 3)
                return false ;
           if(line[0] == "GET"){
                url = line[1] ;
                QFileInfo fi(url) ;
                if(!extensions.contains(fi.suffix())){
                    return false ;
                }
                for(int i = 1; i < list.size() ; i++){
                    line = list[i].split(":") ;
                    if(line.size() < 2)
                        return false ;
                    if(line[0].trimmed() == "Host"){
                        imageurl = line[1].trimmed() + url ;
                        qDebug() << imageurl ;
                        return true ;
                    }
                }

                return false ;
           }
           return false ;
    }
    else{
            return false ;

   }
}

/// \fn QString imagepacket::getImage()
/// \~English
/// \brief Returns an image url if found in an http request
/// \return image url if found in an http request
/// \brief Devuelve un url a una imagen si fue encontrada en una
/// solicitud de http
/// \return url de una imagen si encontrada en una solicitud de http
QString imagepacket::getImage(){
    return imageurl ;
}
