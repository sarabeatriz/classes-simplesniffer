#include "ip_packet.h"

/// \fn ip_packet::ip_packet()
/// \~English
/// \brief Constructor
/// \~Spanish
/// \brief Constructor
ip_packet::ip_packet()
{
    payload = "" ;
    ip_dst = ip_src = "" ;
}

/// \fn void ip_packet::setIPSAddress(string addr)
/// \~English
/// \brief Saves the source IP address in std string format.
/// \param addr source IP address
/// \~Spanish
/// \brief Guarda la direccion IP fuente en formato de cadena de caracteres
/// \param addr direccion IP fuente
void ip_packet::setIPSAddress(string addr){
    ip_src = addr ;
}

/// \fn void ip_packet::setIPDAddress(string addr)
/// \~English
/// \brief Saves the destination IP address in std string format.
/// \param addr destination IP address
/// \~Spanish
/// \brief Guarda la direccion IP destino en formato de cadena de caracteres
/// \param addr direccion IP destino
void ip_packet::setIPDAddress(string addr){
    ip_dst = addr ;
}

/// \fn void ip_packet::setIPProto(char proto)
/// \~English
/// \brief Saves the IP protocol to (TCP|UDP)
/// \param proto protocol number (6 | 17)
/// \~Spanish
/// \brief Guarda el protocolo de IP a (TCP|UDP)
/// \param proto numero de protocolo (6 | 17)
void ip_packet::setIPProto(char proto){
    ip_p = proto ;
}

/// \fn void ip_packet::setIPSPort(unsigned short port)
/// \~English
/// \brief Saves the source port
/// \param port source port
/// \~Spanish
/// \brief Guarda el puerto fuente
/// \param port puerto fuente
void ip_packet::setIPSPort(unsigned short port){
    s_port = port ;
}

/// \fn void ip_packet::setIPDPort(unsigned short port)
/// \~English
/// \brief Saves the destinaation port
/// \param port destination port
/// \~Spanish
/// \brief Guarda el puerto destino
/// \param port puerto destino
void ip_packet::setIPDPort(unsigned short port){
    d_port = port ;
}

/// \fn void ip_packet::setPayload(char *pl, int len)
/// \~English
/// \brief Saves the payload string of bytes into a string.
/// \param pl the packet payload
/// \param len length of the payload
/// \~Spanish
/// \brief Guarda la carga del paquete de una cadena de caracteres a un string.
/// \param pl la carga del paquete
/// \param len el largo del paquete
void ip_packet::setPayload(char *pl, int len){
   for(int i = 0; i < len; i++){
        payload += pl[i] ;
    }
}

/// \fn string ip_packet::getIPSAddress()
/// \~English
/// \brief Returns the source IP address
/// \return the source IP address
/// \~Spanish
/// \brief Devuelve la dirección IP fuente
/// \return la direccion IP fuente
string ip_packet::getIPSAddress(){
    return ip_src ;
}

/// \fn string ip_packet::getIPDAddress()
/// \~English
/// \brief Returns the destination IP address
/// \return the destination IP address
/// \~Spanish
/// \brief Devuelve la dirección IP destino
/// \return la direccion IP destino
string ip_packet::getIPDAddress(){
    return ip_dst ;
}

/// \fn string ip_packet::getPayload()
/// \~English
/// \brief Returns the IP packet payload
/// \return the IP packet payload
/// \~Spanish
/// \brief Devuelve la carga del paquete de IP
/// \return la carga del paquete de IP
string ip_packet::getPayload(){
    return payload ;
}

/// \fn char ip_packet::getIPProto()
/// \~English
/// \brief Returns the IP protocol type
/// \return the IP protocol
/// \~Spanish
/// \brief Devuelve el tipo de protocolo IP
/// \return el protocolo IP
char ip_packet::getIPProto(){
    return ip_p ;
}

/// \fn unsigned short ip_packet::getIPSPort()
/// \~English
/// \brief Returns the source port
/// \return the source port
/// \~English
/// \brief Devuelve el puerto fuente
/// \return el puerto fuente
unsigned short ip_packet::getIPSPort(){
    return s_port ;
}

/// \fn unsigned short ip_packet::getIPDPort()
/// \~English
/// \brief Returns the destination port
/// \return the destination port
/// \~English
/// \brief Devuelve el puerto destino
/// \return el puerto destino
unsigned short ip_packet::getIPDPort(){
    return d_port ;
}
