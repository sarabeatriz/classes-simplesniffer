#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QPixmap>
#include "sniffer.h"
#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <string>
#include <QStringList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QListWidgetItem>
#include <QIcon>
#include "imagepacket.h"

using namespace std ;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Constructor
    /// \~Spanish
    /// \brief Constructor
    explicit MainWindow(QWidget *parent = 0);

    /// \fn MainWindow::~MainWindow()
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~MainWindow();

private slots:

    /// \fn void MainWindow::on_capture_clicked()
    /// \~English
    /// \brief Signal that starts or pause the capture of the packets in the sniffer.
    /// \~Spanish
    /// \brief Senal que inicial o pausa la captura de paquetes en el sniffer
    void on_capture_clicked();

    /// \fn void MainWindow::on_packetDisplay_itemClicked(QTableWidgetItem *item)
    /// \~English
    /// \brief When a line of the packets captured is selected from the GUI
    /// a summary of the payload in ascii is presented in the summary box.
    /// \param item item selected in the GUI
    /// \~Spanish
    /// \brief Cuando una linea de los paquetes es seleccionada desde el GUI
    /// un resumen de la carga en ascii es presentado en la caja de resumen.
    /// \param item articulo seleccionado en el GUI
    void on_packetDisplay_itemClicked(QTableWidgetItem *item);

    /// \fn void MainWindow::on_resetButton_clicked()
    /// \~English
    /// \brief Empties the packets vector, resets the GUI packet table and
    /// summary
    /// \~Spanish
    /// \brief Vacia el vector de paquetes, reajusta la tabla de paquetes y
    /// el resumen del GUI.
    void on_resetButton_clicked();

public slots:

    /// \fn void MainWindow::handleResults(unsigned long index)
    /// \~English
    /// \brief A signal was received from the sniffer that a packet of the form
    /// Ethernet -> IP -> (TCP|UDP) has to be displayed in the GUI.
    /// Also checks if the packet is an http image request to display.
    /// \param index number of the packet in the table
    /// \~Spanish
    /// \brief Una senal es recibida desde el sniffer que un paquete de la forma
    /// Ethernet ->IP -> (TCP|UDP) tiene que ser desplegado en el GUI.
    /// Tambien revisa si el paquete es una solicitud a una imagen en http
    /// para desplegar.
    /// \param index numero de el paquete en la tabla
    void handleResults(unsigned long index) ;

    /// \fn void MainWindow::slot_netwManagerFinished(QNetworkReply *reply)
    /// \~English
    /// \brief This slot is called when an image is successfuly retreived.
    /// The image is then added to the GUI display.
    /// \param reply object that contains the reply to our request.
    /// \~Spanish
    /// \brief Este "slot" es llamado cuando se recibe una imagen con exito.
    /// La imagen entonces es anadida a la vizualizacion en el GUI.
    /// \param reply objeto que contiene la respuesta a nuestra solicitud
    void slot_netwManagerFinished(QNetworkReply *reply) ;
private:
    Ui::MainWindow *ui;
    Sniffer *sniff ; /**< pointer to the sniffer class / apuntador a la clase sniffer */
    QThread sniff_thread ; /**< thread for the sniffer / hilo para el sniffer */
    QWaitCondition wait_pause ; /**< to pause the sniffer / para pausar el sniffer */
    int pause ;                 /**< flag to pause / bandera para pausar */
    QMutex mutex ;              /**< Protect critical region / protege region critica */
    QStringList imageList ;     /**< List of images / lista de imagenes */
    QNetworkAccessManager *netManager ; /**< to request http images / para solicitar imagenes http */
};

#endif // MAINWINDOW_H
